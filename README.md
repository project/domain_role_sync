# Domain role synchronyzation

Simple module that allows to associate a Domain
with one or more roles so when a new user is
created or saved it will also get the roles
associated with the domain if they are affiliated with it.

## Motivation

Some Drupal modules provide functionality based on granular roles
but they don't care about domains so this module can serve as
bridge for functionality that only works with permissions/roles.
It usually requires that you have create a role per domain.

For example [Menu admin per menu](https://www.drupal.org/project/menu_admin_per_menu)
allows you to create a different menu per domain and give permissions to edit each menu to
the editors the role associated with each domain.

## Roadmap

* Remove role when the affiliation with the domain ends.
* Two way synchronyzation: Enable the affiliation to a domain when a user gets a certain role.

## Similar modules

* https://www.drupal.org/project/domain_role_access
