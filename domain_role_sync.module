<?php

/**
 * @file
 * Primary module hooks for Domain role synchronization module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function domain_role_sync_user_presave(UserInterface $user) {
  /** @var \Drupal\domain\Entity\Domain $domain */
  foreach ($user->get('field_domain_access')->referencedEntities() as $domain) {
    $roles = $domain->getThirdPartySetting('domain_role_sync', 'roles');
    if (!is_array($roles)) {
      continue;
    }
    foreach ($roles as $role) {
      $user->addRole($role);
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function domain_role_sync_form_domain_edit_form_alter(&$form, FormStateInterface $form_state) {
  $form['domain_role_sync'] = [
    '#type' => 'details',
    '#open' => FALSE,
    '#summary_attributes' => [],
    '#title' => t('Domain role synchronization'),
  ];

  $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
  $rolesOptions = [];
  foreach ($roles as $roleId => $role) {
    if ($roleId === 'authenticated' || $roleId === 'anonymous') {
      continue;
    }
    $rolesOptions[$roleId] = $role->label();
  }

  /** @var \Drupal\Core\Entity\EntityForm $formObject */
  $formObject = $form_state->getFormObject();
  /** @var \Drupal\domain\Entity\Domain $domain */
  $domain = $formObject->getEntity();
  $form['domain_role_sync']['roles'] = [
    '#type' => 'checkboxes',
    '#title' => t('Roles to automatically assign to users affiliated with this domain'),
    '#title_display' => 'before',
    '#description' => t('When a user has this domain assigned they will also have the enabled roles'),
    '#required' => FALSE,
    '#options' => $rolesOptions,
    '#default_value' => $domain->getThirdPartySetting('domain_role_sync', 'roles') ?: [],
  ];
  array_unshift($form['actions']['submit']['#submit'], 'domain_role_sync_domain_entity_form_submit');
}

/**
 * Form submission handler for roles on the domain entity.
 *
 * @see domain_role_sync_form_domain_edit_form_alter()
 */
function domain_role_sync_domain_entity_form_submit($form, FormStateInterface $form_state) {
  if (!$form_state->isValueEmpty('roles')) {
    /** @var \Drupal\Core\Entity\EntityForm $formObject */
    $formObject = $form_state->getFormObject();
    /** @var \Drupal\domain\Entity\Domain $domain */
    $domain = $formObject->getEntity();
    $values = array_filter($form_state->getValue('roles'));
    if (!$values) {
      $domain->unsetThirdPartySetting('domain_role_sync', 'roles');
      return;
    }
    $domain->setThirdPartySetting('domain_role_sync', 'roles', $values);
  }
}
